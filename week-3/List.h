#include <string>
#include <iostream>
#pragma once
using namespace std;
typedef string ItemType;

class List{
    private:
    struct Node{
        ItemType item;
        Node *next; //pointer
    };
    Node *firstNode;
    int size;


    public:
    //constructor
    List();

    //destructor
    ~List();
    
    //add item to back of list (append)
    bool add(ItemType item);

    //add item at index
    bool add(int index, ItemType item);


    //remove item from list at specific index
    void remove(int index);

    ItemType get(int index);

    bool isEmpty();

    int getLength();

    void print();

};