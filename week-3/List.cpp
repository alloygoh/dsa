#include "List.h"
#include <string>
#include <iostream>
using namespace std;
// constructor
List::List(){
    firstNode = new Node();
    size = 0;
}
List::~List(){
    delete firstNode;
}
// add item to back
bool List::add(ItemType item){
    return add(size, item);
}

bool List::add(int index, ItemType item){
    if(index > size){
        return false;
    }
    else{
        // creating new node to be added
        Node *newNode = new Node;
        newNode->item = item;
        newNode->next = nullptr;
        
        Node *currentNode = firstNode;
        // loop till current node is at index
        while(index-- > 0){
            currentNode = currentNode->next;
        }
        Node *nextNode = currentNode->next; //saved to link later
        currentNode->next = newNode;
        newNode->next = nextNode;
        size++;
        return true;
    }
}

// | | -> alloy -> lol -> null

void List::remove(int index){
    Node *currentNode = firstNode; //set current node to empty 1st node
    while(index-- > 0){
        currentNode = currentNode->next; //navigates to node before index
    }
    Node *nodeDelete = currentNode->next; //set node at index to delete
    currentNode->next = nodeDelete->next;
    size--;
    delete nodeDelete;
}

ItemType List::get(int index){
    Node *currentNode = firstNode; //set current node to empty 1st node
    // navigate to node at index

    // while index >= 0:
    //    index-=1

    while(index-- >= 0){ 
        currentNode = currentNode->next; // navigates to node before index
    }
    return currentNode->item;
}
void List::reverse(){
    Node *current = firstNode;
    Node *next; 
    Node *previous = nullptr;
    while (current != nullptr){
        next = current->next;
        current->next = previous;
        prev = current;
        current = next;
    }
    firstNode = previous;
}

void List::print(){
    Node *printNode = firstNode->next;
    cout << "-----------------" << endl;
    for(int i=0;i<size;i++){
        cout << printNode->item << endl;
        printNode = printNode->next;
    }
    cout << "-----------------\n" << endl;

}

bool List::isEmpty(){
    if(size == 0){
        return true;
    }
    return false;
}

int List::getLength(){
    return size;
}

