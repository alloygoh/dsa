#include "stringstack.h"
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// constructor
StringStack::StringStack(){
    topNode = new Node();
}

// destructor
StringStack::~StringStack(){
    delete topNode;
}

bool StringStack::isEmpty(){
    if(topNode->next == nullptr){
        return true;
    }
    return false;
}

bool StringStack::push(ItemType2 item){
    Node *toAdd = new Node;
    toAdd->item = item;
    toAdd->next = topNode;
    topNode = toAdd;
    return true;
}

bool StringStack::pop(){
    Node *tmp = topNode;
    if (topNode->next == nullptr){
        return false;
    }
    topNode = topNode->next;
    tmp = nullptr;
    delete tmp;
    return true;
}

bool StringStack::pop(ItemType2 &item){
    Node *tmp = topNode;
    if (topNode->next == nullptr){
        return false;
    }
    topNode = topNode->next;
    item = tmp->item;
    delete tmp;
    return true;
}

void StringStack::getTop(ItemType2 &item){
    item = topNode->item;
}

void StringStack::displayInOrder(){
    Node *current = topNode;
    while(current->next != nullptr){
        cout << current->item << endl;
        current = current->next;
    }
}

void StringStack::displayInOrderOfInsertion(){
    Node *current = topNode;
    vector<Node*> nodeVector;
    while(current->next != nullptr){
        nodeVector.push_back(current);
        current = current->next;
    }
    reverse(nodeVector.begin(),nodeVector.end());
    for(int i = 0; i < nodeVector.size(); i++){
        Node *tmp = nodeVector[i];
        cout << tmp->item << endl;
    }
}