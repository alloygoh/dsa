#include "charstack.h"
#include "stringstack.h"
#include <iostream>
#include <string>

using namespace std;

int getPrecedence(char op){
    
    switch (op){
        case '*':
        case '/':{
            return 2;
        }
        case '+':
        case '-':{
            return 1;
        } 
    }
    return 0;
}

string convertToPostfix(string pre){
    CharStack operatorStack;
    string post;
    for(int i=0;i<pre.length();i++){
        char current = pre[i];
        if(isdigit(current)){
            post += current; 
        }
        else if(current == '*' || current == '/' || current == '+' || current == '-' || current == '(' || current == ')'){
            if(operatorStack.isEmpty()){
                operatorStack.push(current);
            }
            else{
                if(current == '('){
                    operatorStack.push(current);
                    continue;
                }
                else if(current == ')'){
                    char open;
                    operatorStack.getTop(open);
                    while(open != '('){
                        operatorStack.pop(open);
                        post+=open;
                        operatorStack.getTop(open);
                    }
                    operatorStack.pop();
                    continue;
                }
                char firstOperator;
                operatorStack.getTop(firstOperator);
                int firstPrecedence = getPrecedence(firstOperator);
                int currentPrecedence = getPrecedence(current);
                //cout << "current: " <<current << "Precede: " << currentPrecedence << endl;
                //cout << "first: " << firstOperator << "precede: " << firstPrecedence<< endl;
                if(firstPrecedence == currentPrecedence){ // if same precedence

                    operatorStack.pop();
                    post += firstOperator;
                    operatorStack.push(current);
                }
                else if(firstPrecedence < currentPrecedence){ // if higher precedence

                    operatorStack.push(current);
                }
                else{ // if lower precedence
                    /*
                    pop until reach same precedence + 1
                    */
                    char topOperator;
                    operatorStack.getTop(topOperator);
                    while(getPrecedence(topOperator) != getPrecedence(current) && !operatorStack.isEmpty()){
                       operatorStack.pop();
                       post += topOperator;
                       operatorStack.getTop(topOperator);
                    }
                    operatorStack.pop();
                    post+= topOperator;
                    operatorStack.push(current);
                }
            }
        }
    }
    char last;
    while(!operatorStack.isEmpty()){
        operatorStack.pop(last);
        post += last;
    }

    return post;
}

int evalPostfix(string postFix){
    StringStack evalStack;
    for(int i=0;i < postFix.length(); i++){
        //cout << "--------stack--------" <<endl;
        //evalStack.displayInOrder();
        //cout << "------end stack------" << endl;
        //cout << "Current: " <<postFix[i] << endl;
        if(isdigit(postFix[i])){
            evalStack.push(string(1,postFix[i]));
        }
        else{
            char operate = postFix[i];
            string first;
            string second;
            evalStack.pop(first);
            evalStack.pop(second);
            int firstNum = stoi(first);
            int secondNum = stoi(second);
            //cout << "first,second = " << first << "," << second << endl;
            //char evaluated;
            switch(operate){
                case '*':{
                    //cout << "Success 1" << endl;
                    string evaluated = to_string(firstNum*secondNum);
                    //evalStack.push(firstNum*secondNum);
                    evalStack.push(evaluated);
                    break;
                }
                case '/':{
                    //cout << "Success 2" <<endl;
                    string evaluated = to_string(secondNum/firstNum);
                    evalStack.push(evaluated);
                    break;
                }
                case '-':{
                    //cout << "Success 3" << endl;
                    string evaluated = to_string(secondNum - firstNum);
                    evalStack.push(evaluated);
                    break;
                }
                case '+':{
                    //cout << "Success 4" << endl;
                    string evaluated = to_string(secondNum + firstNum);
                    evalStack.push(evaluated);
                    break;
                }
            }
        }
    }
    //cout << "-----stack-----" << endl;
    //evalStack.displayInOrder();
    //cout << "---end stack---" << endl;
    string last;
    evalStack.pop(last);
    return stoi(last);
}
bool charset(char c){return (c >= '0' && c <='9') || c == '+' || c == '-' || c=='*' || c=='/' || c=='(' || c==')';}
int main(){
    CharStack s;
    s.push('3');
    s.push('4');
    char toGet;
    s.getTop(toGet);
    cout << "Top of stack: " << toGet << endl;
    cout << "-----Stack-----" << endl;
    s.displayInOrderOfInsertion();
    cout << "---End Stack---" << endl;
    s.pop();
    cout << endl;
    cout << "-----Stack-----" << endl;
    s.displayInOrderOfInsertion();
    cout << "---End Stack---" << endl;

    string infix;
    cout << "infix: ";
    cin >> infix;
    string postFix = convertToPostfix(infix);
    cout << "postfix: " << postFix << endl;
    // filter due to mysterious empty char
    string filtered;
    for(int i=0; i < postFix.length(); i++){
        if(charset(postFix[i])){
            filtered += postFix[i];
        }
    }
    cout << "\nEvaluation: \n" <<evalPostfix(filtered) << endl;
}