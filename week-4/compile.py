#!/usr/bin/env python3

# imports
import sys
import re
import os


def parse(unparsed):
    parsing = re.findall("#include(.*)",unparsed) # extract all includes
    parsing = list(i.replace("\"","") for i in parsing)
    parsing = list(i.strip(" ") for i in parsing)
    parsed = []
    for i in parsing:
        if i[-2:] == ".h":    # extract all .h only
            parsed.append(i)
    return parsed

def read_file(filename):
    try:
        with open(filename) as file:
            code = file.read()
            file.close()
    except FileNotFoundError:
        sys.stderr.write("Header file: {} is not found".format(filename))
    return code

if len(sys.argv) != 2:
    sys.stderr.write("Usage: ./compile.py <name of main.cpp>")
else:
    filename = sys.argv[1]
    try:
        with open(filename) as file:
            main = file.read()
            file.close()
    except FileNotFoundError:
        sys.stderr.write("File specified not in directory.\nCheck and try again.")
        sys.exit()
    headers = parse(main)
    for i in headers:
        read = parse(read_file(i))
        if not read:  # if empty list skip to next set
            next
        else:
            for i in read:
                if i in headers:  # if already in headers, don't add again
                    next
                else:
                    headers.append(i)
    if not headers: # if empty list
        print("No header files found.\nCompiling current file.")
        os.system("g++ {}".format(filename))
    else:
        required = []
        for i in headers:
            required.append(i[:-2] + ".cpp")
        if filename in required:
            print("No other files needed.\nCompiling current file.")
            command = "g++ " + filename
        else:
            command = "g++ "  + " ".join(required) + " {}".format(filename)
        os.system(command)
