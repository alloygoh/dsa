#pragma once
#include <iostream>
#include <string>
using namespace std;

typedef string ItemType2;

class StringStack{
    private:
    struct Node{
        ItemType2 item;
        Node *next;
    };
    Node *topNode;

    public:
    // constructor
    StringStack();
    // destructor
    ~StringStack();
    
    bool isEmpty();

    bool push(ItemType2 item);

    bool pop();

    bool pop(ItemType2 &item);

    void getTop(ItemType2 &item);

    void displayInOrder();

    void displayInOrderOfInsertion();
};