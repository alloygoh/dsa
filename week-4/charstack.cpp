#include "charstack.h"
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// constructor
CharStack::CharStack(){
    topNode = new Node();
}

// destructor
CharStack::~CharStack(){
    delete topNode;
}

bool CharStack::isEmpty(){
    if(topNode->next == nullptr){
        return true;
    }
    return false;
}

bool CharStack::push(ItemType1 item){
    Node *toAdd = new Node;
    toAdd->item = item;
    toAdd->next = topNode;
    topNode = toAdd;
    return true;
}

bool CharStack::pop(){
    Node *tmp = topNode;
    if (topNode->next == nullptr){
        return false;
    }
    topNode = topNode->next;
    tmp = nullptr;
    delete tmp;
    return true;
}

bool CharStack::pop(ItemType1 &item){
    Node *tmp = topNode;
    if (topNode->next == nullptr){
        return false;
    }
    topNode = topNode->next;
    item = tmp->item;
    delete tmp;
    return true;
}

void CharStack::getTop(ItemType1 &item){
    item = topNode->item;
}

void CharStack::displayInOrder(){
    Node *current = topNode;
    while(current->next != nullptr){
        cout << current->item << endl;
        current = current->next;
    }
}

void CharStack::displayInOrderOfInsertion(){
    Node *current = topNode;
    vector<Node*> nodeVector;
    while(current->next != nullptr){
        nodeVector.push_back(current);
        current = current->next;
    }
    reverse(nodeVector.begin(),nodeVector.end());
    for(int i = 0; i < nodeVector.size(); i++){
        Node *tmp = nodeVector[i];
        cout << tmp->item << endl;
    }
}
/*
int getPrecedence(char op){
    
    switch (op){
        case '*':
        case '/':{
            return 2;
        }
        case '+':
        case '-':{
            return 1;
        } 
    }
    return 0;
}

string convertToPostfix(string pre){
    Stack operatorStack;
    string post;
    for(int i=0;i<pre.length();i++){
        char current = pre[i];
        if(isdigit(current)){
            post += current; 
        }
        else if(current == '*' || current == '/' || current == '+' || current == '-'){
            if(operatorStack.isEmpty()){
                operatorStack.push(current);
            }
            else{
                char firstOperator;
                operatorStack.getTop(firstOperator);
                int firstPrecedence = getPrecedence(firstOperator);
                int currentPrecedence = getPrecedence(current);
                //cout << "current: " <<current << "Precede: " << currentPrecedence << endl;
                //cout << "first: " << firstOperator << "precede: " << firstPrecedence<< endl;
                if(firstPrecedence == currentPrecedence){ // if same precedence

                    operatorStack.pop();
                    post += firstOperator;
                    operatorStack.push(current);
                }
                else if(firstPrecedence < currentPrecedence){ // if higher precedence

                    operatorStack.push(current);
                }
                else{ // if lower precedence*/
                    /*
                    pop until reach same precedence + 1
                    */ /*
                    char topOperator;
                    operatorStack.getTop(topOperator);
                    while(getPrecedence(topOperator) != getPrecedence(current) && !operatorStack.isEmpty()){
                       operatorStack.pop();
                       post += topOperator;
                       operatorStack.getTop(topOperator);
                    }
                    operatorStack.pop();
                    post+= topOperator;
                    operatorStack.push(current);
                }
            }
        }
    }
    char last;
    while(!operatorStack.isEmpty()){
        operatorStack.pop(last);
        post += last;
    }

    return post;
}

int evalPostfix(string postFix){
    Stack evalStack;
    for(int i=0;i < postFix.length(); i++){
        cout << "--------stack--------" <<endl;
        evalStack.displayInOrder();
        cout << "------end stack------" << endl;
        if(isdigit(postFix[i])){
            evalStack.push(postFix[i]);
        }
        else{
            char operate = postFix[i];
            char first;
            char second;
            evalStack.pop(first);
            evalStack.pop(second);
            int firstNum = first-48;
            int secondNum = second-48;
            cout << "first,second = " << first << "," << second << endl;
            //char evaluated;
            switch(operate){
                case '*':{
                    cout << "Success 1" << endl;
                    char evaluated = to_string(firstNum*secondNum)[0];
                    //evalStack.push(firstNum*secondNum);
                    evalStack.push(evaluated);
                    break;
                }
                case '/':{
                    cout << "Success 2" <<endl;
                    char evaluated = to_string(secondNum/firstNum)[0];
                    evalStack.push(evaluated);
                    break;
                }
                case '-':{
                    cout << "Success 3" << endl;
                    char evaluated = to_string(secondNum - firstNum)[0];
                    evalStack.push(evaluated);
                    break;
                }
                case '+':{
                    cout << "Success 4" << endl;
                    char evaluated = to_string(secondNum + firstNum)[0];
                    evalStack.push(evaluated);
                    break;
                }
            }
        }
    }
    cout << "--stack--" << endl;
    evalStack.displayInOrder();
    cout << "--end stack--" << endl;
    int final;
    return 0;
}

int main(){
    Stack s;
    s.push('3');
    s.push('4');
    char toGet;
    s.getTop(toGet);
    cout << "Top of stack: " << toGet << endl;
    cout << "-----Stack-----" << endl;
    s.displayInOrderOfInsertion();
    cout << "---End Stack---" << endl;
    s.pop();
    cout << endl;
    cout << "-----Stack-----" << endl;
    s.displayInOrderOfInsertion();
    cout << "---End Stack---" << endl;

    string infix;
    cin >> infix;
    string postFix = convertToPostfix(infix);
    cout << postFix << endl;
    cout << evalPostfix(postFix) << endl;
}*/