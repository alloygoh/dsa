#include <iostream>

using namespace std;

void printBackward(const int array[], int n){
    if(n == 0){
        return;
    }
    else{
        cout << array[n-1] << " ";
        printBackward(array,n-1);
    }

}

int main(){
    int test[7] = {1,18,9,67,90,23,45};
    printBackward(test,7);
    return 0;
}