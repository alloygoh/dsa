#include <string>
#include <iostream>

using namespace std;
long power(int a, int n){
    if(n <= 0){
        return 1;
    }
    else{
        long an = a * power(a,n-1);
        return an;
    }
}

int main(){
    cout << power(2,50) << endl;
    return 0;
}