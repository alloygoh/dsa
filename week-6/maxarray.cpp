#include <iostream>

using namespace std;

/*
int maxArray(const int array[], int n){
    if(n==0){
        cerr << "Empty Array!" << endl;
        return 0;
    }
    else if(n==1){
        //cout << array[0] << endl;
        return array[0];
    }
    else{
        int tmp = maxArray(array, n-1);
        if(tmp > array[n-1]){
            return tmp;
        }
        return array[n-1];
    }
}
*/


int maxArray(const int array[], int start, int end){
    /*
    if(end - start <= 1){
        return array[start];
    }*/
    if(start==end){
        return array[start];
    }
    else{
        int mid  = (start+end)/2;
        int tmp1 = maxArray(array,start, mid);
        int tmp2 = maxArray(array,mid+1,end);
        if(tmp1 > tmp2){
            return tmp1;
        }
        return tmp2;
    }
}

int main(){
    int test[7] = {1,18,9,67,60,23,45};
    int test2[10] = {90,100,2,4,928,76,34,0,12,67};
    cout << maxArray(test,0,6) << endl;
    cout << maxArray(test2,0,9) << endl;
    return 0;
}