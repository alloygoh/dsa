#include <iostream>
#include <string>
#include <limits>

using namespace std;

int main(){
    int x = 0; 
    while(1){
        cout << "Enter a 5 digit number: ";
        cin >> x; //get input
        cin.clear(); //clearing buffer in case of bad input, causing fail state
        cin.ignore(numeric_limits<streamsize>::max(),'\n'); //removing bad input
        if (x && to_string(x).length() == 5){ //if int and length 5
            cout << x/10000 << "   " << (x%10000)/1000 << "   " << (x%1000)/100 << "   " << (x%100)/10 << "   " << x%10;
            break;
        }
    }
    return 0;
}