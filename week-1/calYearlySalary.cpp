#include <string>
#include <iostream>

using namespace std;
class Employee{

    private:
    int salary;
    string first_name;
    string last_name;

    public:
    //constructor
    Employee(string f, string l, int s){
        first_name = f;
        last_name = l;
        salary = s;
    }
    //getter
    string getFirstName() const {return first_name;}
    string getLastName() const {return last_name;}
    int getSalary() const {return salary;}
    //setter
    void setFirstName(string f) {first_name=f;}
    void setLastName(string l) {last_name=l;}
    void setSalary(int s){
        if(s < 0){
            salary = 0;
        }
        else{
            salary = s;
        }
    }
};

int main(){
    // create new employees
    Employee employee1("Ravern","Koh",12);
    Employee employee2("Ashok","Balaji",123);
    // print salary
    cout << "Employee 1 salary: " << employee1.getSalary() << endl;
    cout << "Employee 2 salary: " << employee2.getSalary() << endl;
    // increase salary by 10%
    employee1.setSalary(employee1.getSalary() * 1.1);
    employee2.setSalary(employee2.getSalary() * 1.1);
    // print new salary
    cout << "Employee 1 new salary: " << employee1.getSalary() << endl;
    cout << "Employee 2 new salary: " << employee2.getSalary() << endl;
    return 0;
}
