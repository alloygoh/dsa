# Week 1

## Pointers

To create a pointer, do `datatype* pointername`.

This is used to store the memory address of another variable

To pass a memory address to a pointer, you can do the following

- `int* x = &y` *//where x is a pointer and &y is an address*

`*` - a dereference operator, retrieves the data at address.

`&` - a reference operator, retrieves address of var.

Basically, when `*` is right after the data type of the var name, you are creating a pointer.

When `*` is right infront of the var, you are retrieving the value of the var and the var should be a memory address.

When `&` is right infront of the var, you are retrieving the memory address of the var and should be set to a pointer (e.g. `int* x`)

## Practical on pointers part 2

When x and y is created/initialized, the stack looks something like this.

```
  x     y
-----------
| 30 | 30 |
-----------
 0x2   0x3

```

When the value of x and the address of y(0x3) is inserted into the change function, the stack looks like this.

```
  x     y
-----------
| 30 | 0x3 |
-----------
 0x7   0x8

```

After the function runs, the stack looks like this.

```
  x     y
-----------
| 40 | 0x3 |
-----------
 0x7   0x8

```

The value of the x in address `0x7` is increased by 10, making it 40. However, the original *x* value is not increased as the `+10` only applies to the *x* in the function, namingly the one in `0x7`.

The value of the original *y* is increased to 40 as the address of *y* is passed as a pointer to the second argv of the function.

The code then increases the value the pointer is pointing to, which in this case is `0x3`, making the 30 in `0x3` increase to 40.