#pragma once
class Account
{
    private:
    double balance;

    public:
    Account();
    Account(double b);
    //getter
    double getBalance();
    //setter
    void setBalance(double amount);
    //functions
    virtual void credit(double amount_to_add);
    virtual void debit(double amount_to_remove);
};