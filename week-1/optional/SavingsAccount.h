#pragma once
#include "Account.h"

class SavingsAccount : public Account{
    private:
    double interest_rate;
    public:
    SavingsAccount();
    SavingsAccount(double b, double ir);
    double calculateInterest();
};