#pragma once
#include "Account.h"


class CheckingAccount : public Account{
    private:
    double fee_charged_per_transaction;
    public:
    CheckingAccount();
    CheckingAccount(double b, double f);
    virtual void credit(double amount);
    virtual void debit(double amount);
};