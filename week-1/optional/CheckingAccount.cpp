#include <iostream>
#include <string>
#include "CheckingAccount.h"
using namespace std;
CheckingAccount::CheckingAccount(){};
CheckingAccount::CheckingAccount(double b, double f) : Account(b){
    fee_charged_per_transaction = f;
}
void CheckingAccount::debit(double amount_to_remove){
    if(getBalance() > (fee_charged_per_transaction+amount_to_remove)){
        setBalance(getBalance() - amount_to_remove - fee_charged_per_transaction);
    }

}
void CheckingAccount::credit(double amount_to_add){
    setBalance(getBalance() + amount_to_add - fee_charged_per_transaction);
}
/*
int main(){
    CheckingAccount c1(2,0.5);
    c1.credit(2);
    cout << c1.getBalance() << endl;
    c1.debit(13);
    cout << c1.getBalance() << endl;
    return 0;
}*/