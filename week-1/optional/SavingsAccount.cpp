#include <iostream>
#include <string>
#include "SavingsAccount.h"
using namespace std;

SavingsAccount::SavingsAccount(){}
SavingsAccount::SavingsAccount(double b, double ir) : Account(b) {
    interest_rate = ir;
}
double SavingsAccount::calculateInterest(){
    return (interest_rate)/100 * getBalance();
}
/*
int main(){
    SavingsAccount s1(1,3);
    cout << s1.calculateInterest() << endl;
    return 0;
}*/