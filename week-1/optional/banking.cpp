#include <iostream>
#include <string>
#include "SavingsAccount.h"
#include "CheckingAccount.h"
#include <bits/stdc++.h>
#include <limits>

using namespace std;

int main(){
    SavingsAccount s1(10,2);
    SavingsAccount s2(10,5);
    CheckingAccount c1(10,0.5);
    CheckingAccount c2(10,0.3);
    vector<Account *> accountvec {&s1, &s2, &c1, &c2};
    for (Account* x : accountvec){
        cout << x->getBalance() << endl;
    }
    return 0;
}