#include <iostream>
#include <string>
#include "Account.h"
using namespace std;

Account::Account(){}
Account::Account(double b){balance = b;}
void Account::setBalance(double amount){balance = amount;}
double Account::getBalance(){
    if (balance >= 0){
        return balance;
    }
    else{
        cerr << "Invalid balance. Setting balance to 0" << endl;
        balance = 0;
        return balance;
    }
}
void Account::credit(double amount_to_add){
    balance += amount_to_add;
}
void Account::debit(double amount_to_remove){
    if (balance < amount_to_remove){
        cerr << "Debit amount exceeded account balance" << endl;
    }
    else{
        balance -= amount_to_remove;
    }
}


/*
int main(){
    Account account1(-1);
    cout << account1.getBalance() <<endl;
    return 0;
}*/