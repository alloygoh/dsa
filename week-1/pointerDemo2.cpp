#include <iostream>
#include <string>
using namespace std;

void changeValue(int x, int* y){
    *y += 10; 
    x += 10;
    cout << "Address of pointer y: " << y << endl;
      
}

int main(){
    int x = 30;
    int y;
    y = x;
    cout << "Address of var x: " << &x << endl;
    cout << "Address of var y: " << &y <<endl;
    changeValue(x,&y);
    cout << "Value of x: " << x << endl;
    cout << "Value of y: " << y << endl;
    return 0;
}