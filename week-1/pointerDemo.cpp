#include <iostream>
#include <string>
using namespace std;

// * retrieves value at memory location
// & retreives memory address of var
int main(){
    int value1 = 200000; //set value1
    int value2;
    int* ptr; //create an integer pointer
    ptr = &value1; // setting the integer pointer to the address of value1
    cout << "Value of data ptr is pointing to: " << *ptr << endl;
    value2 = *ptr; //set value2 to data at pointer(address of value1)
    cout << "Value 2: " << value2 << endl;
    cout << "Memory address of Value 1; " << &value1 <<endl; //print memory address of value1
    cout << "Address stored in ptr: " << ptr << endl; //print address stored in ptr (address is stored in ptr as ptr is a pointer)
    ptr = &value2;
    *ptr += 2000;
    cout << "Value 1: " << value1 << endl;
    cout << "Value 2: " << value2 << endl;
}