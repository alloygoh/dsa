//include string lib
#include <string>
using namespace std;

class Person{
    private:
    string name;
    string telno;
    public:
    Person();
    Person(string name, string tele);
    void setName(string name);
    string getName();
    void setTelNo(string name);
    string getTelNo();
};