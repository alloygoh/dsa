//display list
//Add new item
//Remove existing item
//search for item
#include "Person.h"
#pragma once
typedef Person ItemType;

class List{
    private:
    ItemType items[100];
    int size;

    public:
    //constructor
    List();
    
    //add item to back of list (append)
    //pre: NIL
    //post: item is added to back of list and size +=1
    bool add(ItemType item);

    //remove item from list at specific index
    //pre: 1<=index<=size
    //post: item removed at index and size -= 1
    void remove(int index);

    ItemType get(int index);

    int length();

    string display();

};