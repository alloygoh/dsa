#include <iostream>
#include <string>
#include <limits>
#include "List.h"
using namespace std;

void printMenu() {
  cout << "---------------- Main Menu -------------------" << endl;
  cout << "[1] List the phone numbers" << endl;
  cout << "[2] Add a new contact" << endl;
  cout << "[3] Remove a contact" << endl;
  cout << "[4] Search for a phone number" << endl;
  cout << "[0] Exit" << endl;
  cout << "----------------------------------------------" << endl;
}

int getOption(string display){
    while (true){
        int n;
        cout << display;
        cin >> n;
        if (cin.fail()){
            cerr << "Invalid option. Please try again" << endl;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
        return n;
    }
}

string getChoice(string display){
    while(true){
        string s;
        cout << display;
        cin >> s;
        if(cin.fail()){
            cerr << "Invalid string. Please try again" << endl;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
        return s;
    }
}

int main(){
    List l;
    while(true){
        printMenu();
        int n = getOption("Enter your option: ");
        switch(n){
            case 1:{
                if(l.length() == 0){
                    cout << "No Numbers.\n" << endl;
                }
                else{
                    for(int i=0; i<l.length(); i++){
                    cout << l.get(i).getName() << " : " << l.get(i).getTelNo() << endl;
                    }
                }
                break;
            }
            case 2:{
                string name = getChoice("Enter Name: ");
                string tel = getChoice("Enter Number: ");
                Person p(name,tel);
                if(l.add(p)){
                    cout << "Added Successfully" << endl;
                }
                else{
                    cout << "Unsuccessful" << endl;
                }
                break;
            }
            case 3:{
                int index = getOption("Enter index to remove: ");
                l.remove(index);
                break;                
            }
            case 4:{
                string tel = getChoice("Enter the number: ");
                for(int i=0; i<l.length();i++){
                    Person p = l.get(i);
                    if(p.getTelNo() == tel){
                        cout << p.getName() << " : " << p.getTelNo() << " ;Index: " << i << endl;
                        break;
                    }
                }
                break;
            }
            case 0:{
                return 0;
            }
        }
       
    }

    return 0;
}