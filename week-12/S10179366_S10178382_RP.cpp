#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <sstream>
#include <vector>

using namespace std;

string getInput(string outputString){
    string input;
    cout << outputString;
    getline (cin,input);
    return input;
}

void writeToFile1(){
    string filename = getInput("Enter the filename to write to: ");
    cout << filename << endl;
    string toParse = getInput("Enter name of product, quantity and price (seperated by a comma): ");
    cout << toParse << endl;
    ofstream file1; // create output file stream
    file1.open(filename);
    // change all , to tabs to write to file
    replace(toParse.begin(),toParse.end(),',','\t');
    file1 << toParse;
    file1 << "\n";
    file1.close();
}

void readFromFile1(){
    ifstream inFile;
    string Name;
    int Qty;
    double Price;
    inFile.open("product1.txt");
    cout << "======================================" << endl;
    cout << "Name" << "\t\t" << "Qty" << "\t" <<"Price" << "\t" << "Amount" << endl;
    inFile >> Name;
    inFile >> Qty;
    inFile >> Price;
    double Amount = Qty * Price;
    while(!inFile.eof()){
        cout << Name << "\t" << Qty << "\t" << Price << "\t" << Amount << endl;
        inFile >> Name;
        inFile >> Qty;
        inFile >> Price;
        Amount = Qty * Price;
    }
    cout << "======================================" << endl;
    inFile.close();
}

void readWriteFile1(){
    ifstream inFile;
    ofstream ofFile;
    string Name;
    int Qty;
    double Price;
    inFile.open("product1.txt");
    ofFile.open("transaction1.txt");
    inFile >> Name;
    inFile >> Qty;
    inFile >> Price;
    double Amount = Qty * Price;
    while(!inFile.eof()){
        ofFile << Name << "\t" << Qty << "\t" << Price << "\t" << Amount << endl;
        inFile >> Name;
        inFile >> Qty;
        inFile >> Price;
        Amount = Qty * Price;
    }
    inFile.close();
    ofFile.close();
}

void writeToFile2(){
    string product = getInput("Enter the name,quantity and price of product(seperated by a ;): ");
    ofstream file1;
    file1.open("product2.txt",ios::app);
    file1 << product << endl;
    file1.close();
}

void readWriteFile2(){
    ifstream infile {"product2.txt"}; // creating stream to read file
    string contents {istreambuf_iterator<char>(infile),istreambuf_iterator<char>()}; // read content of file from stream
    stringstream ss(contents);
    string pre_parse;
    ofstream transaction;
    transaction.open("transaction2.txt");
    while(getline(ss,pre_parse,'\n')){ // split by line
        //cout << pre_parse << endl;
        vector<string> tokens;
        string token;
        istringstream tokenStream(pre_parse);
        while(getline(tokenStream,token,';')){
            tokens.push_back(token);
        }
        string qty = tokens[1];
        string price = tokens[2];
        string name = tokens[0];
        //cout << name << endl;

        // convert string to int
        stringstream qty_stream(qty);
        stringstream price_stream(price);
        int qty_int = 0;
        int price_int = 0;
        qty_stream >> qty_int;
        price_stream >> price_int;
        /*
        cout << qty_int << endl;
        cout << price_int << endl;*/
        int amount = qty_int * price_int;
        transaction << name << ";" << qty_int << ";" << price_int << ";" << amount << endl;
    }
    transaction.close();
}

int main(){
    while(1){
        string menu = "[1] writeToFile1()\n[2] readFromFile1()\n[3] readWriteFile1()\n[4] writeToFile2()\n[5] readWriteFile2()\nEnter you choice: ";
        cout << menu;
        int option;
        cin >> option;
        cin.ignore();
        if(cin.fail()){
            cin.clear(); //clearing buffer in case of bad input, causing fail state
            cin.ignore(numeric_limits<streamsize>::max(),'\n'); //removing bad input
        }
        switch(option){
            case 1:{
                writeToFile1();
                break;
            } 
            case 2:{
                readFromFile1();
                break;
            }
            case 3:{
                readWriteFile1();
                break;
            }
            case 4:{
                writeToFile2();
                break;
            }
            case 5:{
                readWriteFile2();
                break;
            }
            default:{
                cerr << "Invalid Option" << endl;
            }
        }
    }
}