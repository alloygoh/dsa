#include "charstack.h"
#include "queue.h"
#include <iostream>
#include <string>
using namespace std;

string readInput(){
    cout << "Enter your string: ";
    string input;
    cin >> input;
    return input;
}

bool recognisePalindromes(string ui){ // double free error in this section
    
    Queue q;
    CharStack s;
    for(int i=0;i < ui.length(); i++){
        char current = ui[i];
        q.enqueue(current);
        s.push(current);
    }
    bool equal = true;
    while(!q.isEmpty() && equal){
        char q1;
        char s1;
        s.getTop(s1);
        q.getFront(q1);
        if(s1 == q1){
            s.pop();
            q.dequeue();
        }
        else{
            equal = false;
        }
        //q.displayItems();
        //s.displayInOrder();
    }

    //s.~CharStack();
    //q.~Queue();
    return equal;
}

int main(){
    /*
    Queue q;
    cout << to_string(q.isEmpty()) << endl;
    q.enqueue('a');
    cout << to_string(q.isEmpty()) << endl;
    q.dequeue();
    cout << to_string(q.isEmpty()) << endl;
    */

    
    Queue q;
    //q.displayItems();

    q.enqueue('a');
    q.enqueue('b');
    char top;
    q.getFront(top);
    cout << "Front: " << top << endl;
    q.displayItems();
    q.dequeue();
    q.displayItems();
    string userInput = readInput();
    if(recognisePalindromes(userInput)){
        cout << "Palindromes: True" << endl;
    }
    else{
        cout << "Plaindromes: False" << endl;
    }
    return 0;
    
}
