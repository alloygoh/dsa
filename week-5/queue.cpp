#include <string>
#include <iostream>
#include "queue.h"
using namespace std;


Queue::Queue(){
    frontNode = new Node();
    backNode = new Node();
}

Queue::~Queue(){
    delete frontNode;
    //delete backNode;
}

bool Queue::isEmpty(){
    if(frontNode == NULL || frontNode->item == NULL ){
        return true;
    }
    return false;
}

bool Queue::enqueue(Itemtype item){
    Node *toAdd = new Node;
    toAdd->item = item;
    toAdd->next = nullptr;
    if(isEmpty()){
        frontNode = toAdd;
    }
    else{
        backNode->next = toAdd;
    }
    backNode = toAdd; // shift backnode back
    return true;
}

bool Queue::dequeue(){
    /*
    if(isEmpty()){
        return false;
    }
    else{
        Node *toDelete = frontNode;
        frontNode = frontNode->next; // shift frontnode back
        delete toDelete;
        return true;
    }*/

    if(isEmpty()){
        return false;
    }
    else{
        if(frontNode == backNode){
            frontNode->item = NULL;
            backNode->item = NULL;
        }
        else{
            Node *toDelete = frontNode;
            frontNode = frontNode->next; // shift frontnode back
            toDelete->next = nullptr;
            delete toDelete;
        }
        return true;
    }

}

bool Queue::dequeue(Itemtype &item){
    if(isEmpty()){
        return false;
    }
    else{
        if(frontNode == backNode){
            item = frontNode->item;
            frontNode->item = NULL;
            backNode->item = NULL;
        }
        else{
            Node *toDelete = frontNode;
            frontNode = frontNode->next;
            item = toDelete->item;
            delete toDelete;
        }
        return true;
    }
}

bool Queue::getFront(Itemtype &item){
    if(isEmpty()){
        cout << "Empty" << endl;
        return false;
    }
    else{
        item = frontNode->item;
        return true;
    }
}

void Queue::displayItems(){
    cout << "----Queue----" << endl;
    Node *current = frontNode;
    while(current != nullptr){
        // cout << "success" << endl;
        cout << current->item << endl;
        current = current->next;
    }
    cout << "--End Queue--" << endl;

}
