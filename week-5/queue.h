#pragma once
#include <string>
#include <iostream>

using namespace std;

typedef char Itemtype;

class Queue{
    private:
    struct Node{
        Itemtype item;
        Node *next;
    };
    Node *frontNode;
    Node *backNode;

    public:
    // constructor
    Queue();

    // destructor
    ~Queue();

    bool enqueue(Itemtype item);

    bool dequeue();
    
    bool dequeue(Itemtype &item);

    bool getFront(Itemtype &item);

    bool isEmpty();

    void displayItems();
};
