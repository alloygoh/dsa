#include "charstack.h"
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// constructor
CharStack::CharStack(){
    topNode = new Node();
}

// destructor
CharStack::~CharStack(){
    delete topNode;
}

bool CharStack::isEmpty(){
    if(topNode->next == nullptr){
        return true;
    }
    return false;
}

bool CharStack::push(ItemType1 item){
    Node *toAdd = new Node;
    toAdd->item = item;
    toAdd->next = topNode;
    topNode = toAdd;
    return true;
}

bool CharStack::pop(){
    Node *tmp = topNode;
    if (topNode->next == nullptr){
        return false;
    }
    topNode = topNode->next;
    tmp = nullptr;
    delete tmp;
    return true;
}

bool CharStack::pop(ItemType1 &item){
    Node *tmp = topNode;
    if (topNode->next == nullptr){
        return false;
    }
    topNode = topNode->next;
    item = tmp->item;
    delete tmp;
    return true;
}

void CharStack::getTop(ItemType1 &item){
    item = topNode->item;
}

void CharStack::displayInOrder(){
    Node *current = topNode;
    cout << "----stack----" << endl;
    while(current->next != nullptr){
        cout << current->item << endl;
        current = current->next;
    }
    cout << "--End stack--" << endl;
}
  