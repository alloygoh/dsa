#pragma once
#include <iostream>
#include <string>
using namespace std;

typedef char ItemType1;

class CharStack{
    private:
    struct Node{
        ItemType1 item;
        Node *next;
    };
    Node *topNode;

    public:
    // constructor
    CharStack();
    // destructor
    ~CharStack();
    
    bool isEmpty();

    bool push(ItemType1 item);

    bool pop();

    bool pop(ItemType1 &item);

    void getTop(ItemType1 &item);

    void displayInOrder();

};